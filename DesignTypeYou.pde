
PFont font;  // create a PFont object
int count = 0;
float ypos = 0; 
float xpos =0;

PImage img;
int pointillize = 8;


void setup( ) { 
  size (800, 800); 
  background (0); 

  img = loadImage("pfp1to1800by800.png");
  String words = loadStrings("words.txt")[0].toUpperCase();
  int wordnum = 0;
  
  // load and select the font to use
  font = loadFont ("CourierNewPSMT-48.vlw");
  textFont (font, 20);


  for (int i = 0; i < 800; i += 11) {
    for (int j = 0; j < 800; j += 11) {

      int y = i;
      int x = j;
      int coordinates = x + y*img.width;


      // Look up the RGB color in the source image pixel
      loadPixels();
      float r = red(img.pixels[coordinates]);
      float g = green(img.pixels[coordinates]);
      float b = blue(img.pixels[coordinates]);
      noStroke();

  

      // Draw a letter at that coordinates with that color of the chosen pixel
      fill(r, g, b, 100);
      String letter = String.valueOf(words.charAt(wordnum%words.length()));
      text (letter, x, y);
      wordnum ++;
    }
  }
  //save("final.png");
} 
